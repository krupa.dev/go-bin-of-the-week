FROM golang:1.16-alpine AS builder
WORKDIR /src
COPY . .
RUN go build -ldflags="-extldflags=-static" -tags osusergo,netgo -o /out/app .

FROM scratch
COPY --from=builder /out/app /app
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
USER 1000:1000
ENTRYPOINT ["/app"]
