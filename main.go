package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/krupa.dev/go-bin-of-the-week/coventry"
	"gitlab.com/krupa.dev/go-bin-of-the-week/push"
)

type NotifySpec struct {
    Street string
    Code   string
}

func main() {
    fmt.Println("Start Router")
    r := mux.NewRouter()
    r.HandleFunc("/street", nextBins).Methods("GET")
    r.HandleFunc("/street-readable", nextBinsReadable).Methods("GET")
    r.HandleFunc("/actuator/health", health).Methods("GET")
    r.HandleFunc("/notify", pushBins).Methods("POST")
    fmt.Println("Start Server")
    http.ListenAndServe(":8080", r)
}

func health(w http.ResponseWriter, r *http.Request) {
    w.WriteHeader(200)
    fmt.Fprint(w, "OK")
}

func pushBins(w http.ResponseWriter, r *http.Request) {
    fmt.Println("Ping")
    var body NotifySpec
    dec := json.NewDecoder(r.Body)
    dec.DisallowUnknownFields()
    err := dec.Decode(&body)
    if err == nil && body.Street != "" {
        if os.Getenv("SECURITY_CODE") == body.Code {
            bins := binsForStreet(body.Street)
            fmt.Println(body.Street)
            push.PushMessage(fmt.Sprintf("It's bin day tomorrow. You need to put out the following bins: %s",
                strings.Join(bins, ", ")))
            w.WriteHeader(200)
            fmt.Fprint(w, "\"OK\"")
        } else {
            w.WriteHeader(401)
            fmt.Fprint(w, "\"Access Denied\"")
        }
    } else if err == nil {
        w.WriteHeader(400)
        fmt.Fprint(w, "No street specified")
    } else {
        w.WriteHeader(400)
        fmt.Fprint(w, err.Error())
    }
}

func binsForStreet(streetName string) []string {
    streetUrl := coventry.StreetPage(streetName)
    if streetUrl != "" {
        fmt.Printf("Street URL: %s\n", streetUrl)
        collectionUrl := coventry.CollectionUrl(streetUrl)
        if collectionUrl != "" {
            fmt.Printf("Collection URL: %s\n", collectionUrl)
            dates := coventry.Dates(collectionUrl)
            bins := coventry.NextBins(dates)
            return bins
        }
    }
    return []string{}
}

func nextBins(w http.ResponseWriter, r *http.Request) {
    streetName := r.URL.Query().Get("street")
    bins := binsForStreet(streetName)
    text, err := json.Marshal(bins)
    if err == nil {
        w.WriteHeader(200)
        w.Write(text)
    } else {
        w.WriteHeader(400)
        fmt.Fprint(w, err.Error())
    }
}

func map2(data []string, f func(string) string) []string {

    mapped := make([]string, len(data))

    for i, e := range data {
        mapped[i] = f(e)
    }

    return mapped
}

func nextBinsReadable(w http.ResponseWriter, r *http.Request) {
    streetName := r.URL.Query().Get("street")
    bins := strings.Join(
        map2(binsForStreet(streetName), func(str string) string {
            return strings.Title(str) 
        }), " and ",
    )
    text, err := json.Marshal(bins)
    if err == nil {
        w.WriteHeader(200)
        w.Write(text)
    } else {
        w.WriteHeader(400)
        fmt.Fprint(w, err.Error())
    }
}
