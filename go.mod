module main

go 1.16

replace gitlab.com/krupa.dev/go-bin-of-the-week/coventry => ./coventry

replace gitlab.com/krupa.dev/go-bin-of-the-week/push => ./push

require (
	github.com/gorilla/mux v1.8.0
	github.com/robfig/cron/v3 v3.0.1
	gitlab.com/krupa.dev/go-bin-of-the-week/coventry v0.0.0-00010101000000-000000000000
	gitlab.com/krupa.dev/go-bin-of-the-week/push v0.0.0-00010101000000-000000000000
)
