SOURCES=$(find . -name "*.go")

all: ${SOURCES}
	go build -o bin/app .
