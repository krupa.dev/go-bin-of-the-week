package push

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

func PushMessage(message string) {
	username := os.Getenv("PUSHOVER_USERNAME")
	password := os.Getenv("PUSHOVER_TOKEN")

	apiUrl := "https://api.pushover.net"
	resource := "/1/messages.json"
	data := url.Values{}
	data.Set("user", username)
	data.Set("token", password)
	data.Set("message", message)

	u, _ := url.ParseRequestURI(apiUrl)
	u.Path = resource
	urlStr := u.String()

	client := &http.Client{}
	r, _ := http.NewRequest(http.MethodPost, urlStr, strings.NewReader(data.Encode()))
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	resp, _ := client.Do(r)
	fmt.Println(resp.Status)
	fmt.Println(message)
}
