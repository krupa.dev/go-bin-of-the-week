package coventry

import (
	"fmt"
	"log"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

const baseUrl = "https://www.coventry.gov.uk/directory/search"

type Collection struct {
	date    time.Time
	colours []string
}

func StreetPage(streetName string) string {
	url, _ := url.Parse(baseUrl)
	q := url.Query()
	q.Add("directoryID", "82")
	q.Add("keywords", streetName)
	q.Add("search", "Go")
	url.RawQuery = q.Encode()

	fmt.Printf("StreetPage URL is %s\n", url.String())

	doc, err := goquery.NewDocument(url.String())
	if err != nil {
		log.Fatal(err)
		return ""
	} else {
		subUrl, exists := doc.Find("ul.list--record>li>a").Attr("href")
		if exists {
			return subUrl
		} else {
			return ""
		}
	}
}

func CollectionUrl(streetUrl string) string {
	url, _ := url.Parse(baseUrl)
	url.Path = streetUrl

	fmt.Printf("Collection URL is %s\n", url.String())

	doc, err := goquery.NewDocument(url.String())
	if err != nil {
		log.Fatal(err)
		return ""
	} else {
		subUrl, exists := doc.Find("div.editor>p>a").Attr("href")
		if exists {
			fmt.Printf("Found the subUrl: %s\n", subUrl)
			return subUrl
		} else {
			return ""
		}
	}
}

func Dates(collectionUrl string) (ret []Collection) {
	url, _ := url.Parse(collectionUrl)
	url.Scheme = "https"
	monthRegex, _ := regexp.Compile("[A-Z][a-z]+ [0-9]{4}")
	binRegex, _ := regexp.Compile("([a-z]+)-lidded")

	fmt.Printf("Collection Dates URL is %s\n", url.String())

	doc, err := goquery.NewDocument(url.String())
	if err != nil {
		log.Fatal(err)
	} else {
		doc.Find("h2").FilterFunction(func(i int, selection *goquery.Selection) bool {
			return monthRegex.MatchString(selection.Text())
		}).Each(func(i int, selection *goquery.Selection) {
			monthText := strings.TrimSpace(selection.Text())
			fmt.Printf("Found a matching selection for %s\n", monthText)
			year := strings.Split(monthText, " ")[1]
			fmt.Printf("Year: %s\n", year)
			selection.Parent().Find("ul>li").Each(func(i int, selection *goquery.Selection) {
				fmt.Printf("Found list item for %s\n", selection.Text())
				fullDate := fmt.Sprintf("%s %s", strings.Split(selection.Text(), ":")[0], year)
                fullDate = strings.ReplaceAll(fullDate, "\xc2\xa0", " ")
                fmt.Printf("Found full date: <%s>\n", fullDate)
				parsedDate, err := time.ParseInLocation("Monday 2 January 2006", fullDate, time.Local)
                if err == nil {
				    fmt.Printf("Parsed date is %s\n", parsedDate)
                } else {
                    log.Fatal(err)
                }
				bins := selection.Find("a").Map(func(i int, selection *goquery.Selection) string {
					matches := binRegex.FindAllStringSubmatch(selection.Text(), 1)
					fmt.Printf("Found matches %s\n", matches)
					if len(matches) > 0 {
						return matches[0][1]
					} else {
						return ""
					}
				})
				ret = append(ret, Collection{
					date:    parsedDate,
					colours: bins,
				})
			})

		})
	}
	for _, element := range ret {
		fmt.Printf("{date: %s, bins: %s}\n", element.date, element.colours)
	}
	return
}

func NextBins(collections []Collection) []string {
	now := time.Now().Local()
	nextWeek := now.AddDate(0, 0, 7)
	for _, coll := range collections {
		fmt.Printf("Comparing %s <= %s < %s\n",
			now.String(), coll.date.String(), nextWeek.String())
		if now.Unix() <= coll.date.Unix() && nextWeek.Unix() > coll.date.Unix() {
			fmt.Printf("Matched!!\n")
			return coll.colours
		}
	}
	return []string{}
}
